# Yourpass Writer

This component allows you to download passes using the YourPass API.
**Table of contents:**  
  
[TOC]

## Authorization
This component utilizes the password credentials method of authorization. To be able to use the writer the following must be 
included in the authorization configuration: 

- Username (username) - [REQ]
- Password (#password) - [REQ]
- Client Id (client_id) - [REQ]
- Client secret (#client_secret) - [OPT] If not specified, can be left empty

Additionally you can also input if you want to write to sandbox mode 

- Sandbox (sandbox) - [OPT] checkbox of whether to use the sandbox mode URI

## Row configuration
In each row configuration these variables are set :

- Endpoint (endpoint) - [REQ] Which endpoint to perform the action on ; pass, template ..
- Output name (output_name) - [REQ] Name of output table in Keboola  
- Template Id (template_id) - [OPT] which passes from template Id to fetch
- Project Id (template_id) - [OPT] which project to fetch passes from
- Loading options (loading_options_) - [REQ] What type of loading; Full overwrites the existing table in storage and incremental appends new and updates existing passes in the table using a primary key.
  - Primary Key - [OPT] Necessary for incremental load type
