import csv
import logging
from datetime import datetime
from typing import List
from yourpass import YourPassClient, LoginError

from keboola.component.base import ComponentBase, UserException

KEY_TEMPLATE_ID = 'template_id'
KEY_PROJECT_ID = 'project_id'
KEY_SANDBOX = "sandbox"
KEY_USERNAME = "username"
KEY_PASSWORD = "#password"
KEY_CLIENT_ID = "client_id"
KEY_CLIENT_SECRET = "client_secret"
KEY_OUTPUT_NAME = "output_name"
KEY_LOADING_OPTIONS = "loading_options"
KEY_LOADING_OPTIONS_INCREMENTAL = "incremental"
KEY_LOADING_OPTIONS_INCREMENTAL_FETCH = "incremental_fetch"
KEY_LOADING_OPTIONS_PKEY = "pkey"

INCREMENTAL_FIELD = "updatedAt"

REQUIRED_PARAMETERS = [KEY_USERNAME, KEY_PASSWORD, KEY_CLIENT_ID, KEY_OUTPUT_NAME]
REQUIRED_IMAGE_PARS = []


class Component(ComponentBase):
    def __init__(self):
        super().__init__(required_parameters=REQUIRED_PARAMETERS,
                         required_image_parameters=REQUIRED_IMAGE_PARS)

    def run(self):
        params = self.configuration.parameters
        loading_options = params.get(KEY_LOADING_OPTIONS)
        template_id = params.get(KEY_TEMPLATE_ID)
        project_id = params.get(KEY_PROJECT_ID)
        sandbox = params.get(KEY_SANDBOX)
        username = params.get(KEY_USERNAME)
        password = params.get(KEY_PASSWORD)
        client_id = params.get(KEY_CLIENT_ID)
        client_secret = params.get(KEY_CLIENT_SECRET, "")

        incremental = loading_options.get(KEY_LOADING_OPTIONS_INCREMENTAL)
        pkey = loading_options.get(KEY_LOADING_OPTIONS_PKEY)
        incremental_fetch = loading_options.get(KEY_LOADING_OPTIONS_INCREMENTAL_FETCH)

        previous_state = self.get_state_file()
        prev_run = previous_state.get('prev_run')

        client = self.get_yourpass_client(client_id, client_secret, username, password, sandbox)

        if incremental_fetch:
            yp_passes, columns = client.get_passes(template_id, project_id, last_incremental_fetch=prev_run)
        else:
            yp_passes, columns = client.get_passes(template_id, project_id)

        if not columns:
            columns = previous_state.get('prev_columns')

        output_name = params.get(KEY_OUTPUT_NAME)
        self.validate_table_name(output_name)

        if columns and yp_passes:
            table = self.create_out_table_definition(output_name,
                                                     incremental=incremental,
                                                     primary_key=pkey,
                                                     columns=columns)
            self.write_results(table.full_path, yp_passes, columns)
            self.write_tabledef_manifest(table)
            current_timestamp = str(datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S.00000Z'))
            self.write_state_file({"prev_run": current_timestamp, "prev_columns": columns})
        else:
            logging.warning("No data found using this configuration, please make sure passes exist")

    @staticmethod
    def get_yourpass_client(client_id: str, client_secret: str, username: str, password: str,
                            sandbox: bool) -> YourPassClient:
        try:
            yourpass_client = YourPassClient(sandbox)
            yourpass_client.authenticate_client(client_id, client_secret, username, password)
            return yourpass_client
        except LoginError as login_error:
            raise UserException(login_error) from login_error

    @staticmethod
    def write_results(table_path: str, passes: List[dict], columns: List):
        with open(table_path, mode='wt', encoding='utf-8', newline='') as out_file:
            writer = csv.DictWriter(out_file, columns)
            for yp_pass in passes:
                writer.writerow(yp_pass)

    @staticmethod
    def validate_table_name(table_name: str):
        if not table_name.replace("_", "").isalnum():
            raise UserException(
                "Output Table name is not valid, make sure it only contains alphanumeric characters and underscores")


if __name__ == "__main__":
    try:
        comp = Component()
        comp.run()
    except UserException as exc:
        logging.exception(exc)
        exit(1)
    except Exception as exc:
        logging.exception(exc)
        exit(2)
