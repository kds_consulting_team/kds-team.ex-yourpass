import requests
import base64
import math
from typing import List, Generator

PROD_URL = "https://api.yourpass.eu/"
SANDBOX_URL = "https://pass.ysplay.cz/api/"
AUTH_ENDPOINT = "oauth2/token"
PASS_ENDPOINT = "v1/pass"
ACCESS_TOKEN_KEY = "access_token"
PAGE_LIMIT = 100

PASS_KEYS = ['id', 'url', "dynamicData", 'voided', 'expirationDate', 'createdAt', 'updatedAt', 'deletedAt',
             'firstRegisterAt',
             'lastRegisterAt', 'firstUnregisterAt', 'lastUnregisterAt', 'templateId', 'projectId', 'devices']


class LoginError(Exception):
    pass


class YourPassClient:
    def __init__(self, sandbox):
        self.api_url = PROD_URL
        if sandbox:
            self.api_url = SANDBOX_URL
        self.access_token = ""
        self.request_header = {}

    def authenticate_client(self, client_id: str, client_secret: str, username: str, password: str):
        self.access_token = self.get_access_token(client_id, username, password, client_secret)
        self.request_header = self.get_request_header()

    def get_request_header(self) -> List:
        auth_header = " ".join(["Bearer", self.access_token])
        header = {"authorization": auth_header,
                  "content-type": "application/json"}
        return header

    def get_access_token(self, client_id: str, username: str, password: str, client_secret: str) -> str:
        client_header = self.get_client_header(client_id, client_secret)
        header = {"Authorization": client_header,
                  "Cache-Control": "no-cache",
                  "Content-Type": "application/x-www-form-urlencoded"}
        data = {"grant_type": "password",
                "username": username,
                "password": password}
        auth_url = "".join([self.api_url, AUTH_ENDPOINT])
        auth_request = requests.post(auth_url, headers=header, data=data)
        if auth_request.status_code == 200:
            response = auth_request.json()
            return response[ACCESS_TOKEN_KEY]
        elif auth_request.status_code == 400:
            raise LoginError(
                "Failed to authorize extraction, please recheck your password, username, client Id and secret")

    @staticmethod
    def get_client_header(client_id: str, client_secret: str) -> str:
        client_header = ":".join([client_id, client_secret])
        encoded_client_header = YourPassClient.encode_to_base_64(client_header)
        return " ".join(["Basic", encoded_client_header])

    @staticmethod
    def encode_to_base_64(string_to_encode: str) -> str:
        string_to_encode = string_to_encode.encode('ascii')
        encoded_string = base64.b64encode(string_to_encode).decode('ascii')
        return encoded_string

    def get_passes(self, template_id: str, project_id: str, last_incremental_fetch: str = "") -> (List[dict], List):

        session = requests.Session()
        passes = []
        pass_list_pages = self._get_pass_list_pages(session, template_id, project_id, last_incremental_fetch)
        for pass_page in pass_list_pages:
            if pass_page["data"]:
                passes.extend(pass_page["data"])
        passes = self.flatten_results(passes)
        columns = []
        if passes:
            columns = passes[0].keys()
        return passes, list(columns)

    def _get_pass_list_pages(self, session: requests.Session, template_id: str, project_id: str,
                             last_incremental_fetch: str) -> Generator:

        where_clause = self.get_where_clause(template_id, project_id, last_incremental_fetch)
        params = {"page": 1, "limit": PAGE_LIMIT, "where": where_clause}
        pass_url = "".join([self.api_url, PASS_ENDPOINT])
        first_page = requests.get(pass_url, params=params, headers=self.request_header).json()
        yield first_page
        num_pages = 0
        if 'totalCount' in first_page:
            num_pages = math.ceil(first_page['totalCount'] / PAGE_LIMIT)

        for page in range(2, num_pages + 1):
            params = {"page": page, "limit": PAGE_LIMIT, "where": where_clause}
            next_page = session.get(pass_url, params=params, headers=self.request_header).json()
            yield next_page

    @staticmethod
    def get_where_clause(template_id: str, project_id: str, last_incremental_fetch: str) -> str:
        where_clause = "{"
        voided_query = "\"voided\":{\"$eq\":\"False\"}"
        where_clause = "".join([where_clause, voided_query])
        if template_id:
            template_id_clause = "".join([", \"templateId\":{\"$eq\":\"", template_id, "\"}"])
            where_clause = "".join([where_clause, template_id_clause])
        if project_id:
            project_id_clause = "".join([", \"projectId\":{\"$eq\":\"", project_id, "\"}"])
            where_clause = "".join([where_clause, project_id_clause])
        if last_incremental_fetch:
            last_incremental_fetch_clause = "".join([", \"updatedAt\":{\"$gt\":\"", last_incremental_fetch, "\"}"])
            where_clause = "".join([where_clause, last_incremental_fetch_clause])

        return "".join([where_clause, "}"])

    def flatten_result(self, json_dict: dict, delim: str = "_") -> dict:
        flattened_dict = {}
        for main_key in json_dict.keys():
            if isinstance(json_dict[main_key], dict):
                get = self.flatten_result(json_dict[main_key], delim)
                for sub_key in get.keys():
                    new_key = "".join([main_key, delim, sub_key])
                    flattened_dict[new_key] = get[sub_key]
            else:
                flattened_dict[main_key] = json_dict[main_key]

        return flattened_dict

    def flatten_results(self, result_list: List[dict]) -> List[dict]:
        return [self.flatten_result(result) for result in result_list]
